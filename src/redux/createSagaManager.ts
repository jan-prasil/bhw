import { SagaIterator, Task } from 'redux-saga'
import { all, call, cancel, fork, put, select, take } from 'redux-saga/effects'

import fetchConfigSaga from './config/sagas'
import { configActions, configSelectors } from './config'
import { getNoteSagas } from './notes'
import { axiosWrapper, createApi } from '../api'
import { SagaManager, StoreConfiguration } from '../types'

// action type which cancels a running rootSaga in cancellableSaga
export const CANCEL_ROOT_SAGA = 'CANCEL_ROOT_SAGA'

// business logic saga entry point
export function* rootSaga(): SagaIterator {
  const apiUrl = yield select(configSelectors.getApiUrl)
  const apiSagas = createApi(
    axiosWrapper({
      apiUrl,
    })
  )

  yield all([...getNoteSagas(apiSagas)])
}

function* initAppSaga(): Generator {
  yield call(fetchConfigSaga)
  // start the root saga
  const task = (yield fork(rootSaga)) as Task
  yield put(configActions.rootSagaStarted())
  yield take(CANCEL_ROOT_SAGA)
  yield cancel(task)
}

function createSagaManager({ sagaMiddleware, store }: StoreConfiguration): SagaManager {
  return {
    cancel: () => store.dispatch({ type: CANCEL_ROOT_SAGA }),
    start: () => sagaMiddleware.run(initAppSaga),
  }
}

export default createSagaManager
