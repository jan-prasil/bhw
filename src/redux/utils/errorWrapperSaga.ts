import { push } from 'connected-react-router'
import i18next from 'i18next'
import { call, cancelled, put } from 'redux-saga/effects'

import { pendingActions } from '../pending'

function* errorWrapper(saga: (...args: any[]) => any, ...params: Array<any>): Generator {
  const type = (params.length ? params[params.length - 1] : {}).type || 'OTHER'
  try {
    yield put(pendingActions.addPendingSaga(type))
    const result = yield call(saga, ...params)
    yield put(pendingActions.removePendingSaga(type))
    return result
  } catch (error) {
    yield put(pendingActions.removePendingSaga(type))
    yield put(push(i18next.t('routes.error')))
    // eslint-disable-next-line
    console.log(`SagaError ${type}`, error)
  } finally {
    if (yield cancelled()) yield put(pendingActions.removePendingSaga(type))
  }
  return null
}

export default errorWrapper
