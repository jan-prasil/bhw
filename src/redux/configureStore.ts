import { routerMiddleware } from 'connected-react-router'
import { History } from 'history'
import { applyMiddleware, compose, createStore, StoreEnhancer } from 'redux'
import createSagaMiddleware, { SagaMiddleware } from 'redux-saga'

import createReducer from './createReducer'
import { MutableState, StoreConfiguration } from '../types'

interface ConfigureStoreOptions {
  history: History<unknown>
}

const getComposeEnhancer = (): (<R>(a: R) => StoreEnhancer) => {
  try {
    if (localStorage.getItem('devtools') === 'enable') {
      // eslint-disable-next-line
      return window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ as (<R>(a: R) => StoreEnhancer) || compose
    }
    return compose
  } catch (e) {
    return compose
  }
}

const configureStore = (
  options: ConfigureStoreOptions,
  initialState?: MutableState
): StoreConfiguration => {
  const middlewares = []
  const sagaMiddleware: SagaMiddleware = createSagaMiddleware()
  middlewares.push(routerMiddleware(options.history))
  middlewares.push(sagaMiddleware)

  const store = createStore(
    createReducer({ history: options.history }),
    initialState,
    getComposeEnhancer()(applyMiddleware(...middlewares))
  )

  return { store, sagaMiddleware }
}

export default configureStore
