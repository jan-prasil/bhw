import * as configActions from './reducer'
import * as configSagas from './sagas'
import * as configSelectors from './selectors'

export { configActions, configSagas, configSelectors }

export default configActions.default
