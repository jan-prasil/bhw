import get from 'lodash/get'

import { State } from '../../types'

export const getApiUrl = (state: State): string => get(state, 'config.apiUrl')

export const getRootSagaStarted = (state: State): boolean =>
  get(state, 'config.rootSagaStarted', false)
