import { Action, createActions, handleActions } from 'redux-actions'

import initialState from './model'
import { ActionCreator, Config, ImmutableConfigState } from '../../types'

const actions = createActions('ROOT_SAGA_CANCELED', 'ROOT_SAGA_STARTED', 'SET_API_URL')

export const { rootSagaCanceled, rootSagaStarted, setApiUrl }: ActionCreator = actions
export default handleActions<ImmutableConfigState>(
  {
    [setApiUrl]: (state, action: Action<Config>) => state.set('apiUrl', action.payload.apiUrl),
    [rootSagaCanceled]: (state) => state.set('rootSagaStarted', false),
    [rootSagaStarted]: (state) => state.set('rootSagaStarted', true),
  },
  initialState
)
