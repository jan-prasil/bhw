import Immutable from 'seamless-immutable'

import { ConfigState, ImmutableConfigState } from '../../types'

const model: ConfigState = {
  apiUrl: '',
  rootSagaStarted: false,
}

const initialState: ImmutableConfigState = Immutable.from(model)
export default initialState
