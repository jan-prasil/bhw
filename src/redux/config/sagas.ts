import axios, { AxiosResponse, AxiosStatic } from 'axios'
import { call, put } from 'redux-saga/effects'

import * as configActions from './reducer'
import { endpoints } from '../../constants'

export default function* fetchConfigSaga(): Generator {
  try {
    const config = (yield call(
      (axios as AxiosStatic).get,
      endpoints.fetchConfig()
    )) as AxiosResponse
    if (config.status === 200) {
      yield put(configActions.setApiUrl(config.data))
    }
  } catch (e) {
    console.error(e) // eslint-disable-line
    throw new Error('Error fetching config file')
  }
}
