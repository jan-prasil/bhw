import { call } from 'redux-saga/effects'
import { expectSaga } from 'redux-saga-test-plan'

import { createNote } from './sagas'

const mockApi = {
  postNote: jest.fn(),
}

describe('note saga', () => {
  it('can create a note', () => {
    const form = {}
    const resolve = jest.fn()
    const action = {
      payload: {
        form,
        resolve,
      },
    }
    expectSaga(createNote, mockApi, action)
      .provide([[call(mockApi.postNote), action.payload.form]])
      .run()
    expect(resolve).toBeCalled()
    expect(mockApi.postNote).toBeCalled()
  })
})
