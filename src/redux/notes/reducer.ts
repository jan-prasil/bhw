import { createActions, handleActions } from 'redux-actions'

import initialState from './model'
import { Action, ActionCreator, ImmutableNoteState } from '../../types'

const actions = createActions(
  {
    SET_NOTE: [(payload) => payload, (payload, meta) => meta],
    UPDATE_NOTE: [(payload) => payload, (payload, meta) => meta],
  },
  'CREATE_NOTE',
  'FETCH_NOTE',
  'REMOVE_NOTE',
  'FETCH_NOTES',
  'SET_NOTES'
)

export const {
  createNote,
  updateNote,
  fetchNote,
  fetchNotes,
  removeNote,
  setNote,
  setNotes,
}: ActionCreator = actions

export default handleActions<ImmutableNoteState>(
  {
    [setNote]: (state, action: Action<any, { id: number }>) =>
      action.meta?.id ? state.setIn(['items', action.meta.id.toString()], action.payload) : state,
    [setNotes]: (state, action: Action<any>) => state.set('noteList', action.payload),
  },
  initialState
)
