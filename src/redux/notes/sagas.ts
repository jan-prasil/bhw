import { call, put } from 'redux-saga/effects'

import * as actions from './reducer'
import { Action, ApiDefinition, ExactAction, NoteAction, NoteDeleteAction, Saga } from '../../types'
import errorWrapper from '../utils/errorWrapperSaga'

export function* createNote(api: ApiDefinition, action: ExactAction<NoteAction>): Saga {
  const response = yield call(api.postNote, action.payload.form)
  action.payload.resolve(response)
}
export function* fetchNote(api: ApiDefinition, action: ExactAction<number>): Saga {
  const response = yield call(api.fetchNoteById, action.payload)
  yield put(actions.setNote(response, { id: action.payload }))
}
export function* fetchNotes(api: ApiDefinition): Saga {
  const response = yield call(api.fetchNotes)
  yield put(actions.setNotes(response))
}
export function* removeNote(api: ApiDefinition, action: ExactAction<NoteDeleteAction>): Saga {
  yield call(api.deleteNote, action.payload.id)
  action.payload.resolve()
}
export function* updateNote(api: ApiDefinition, action: ExactAction<NoteAction, number>): Saga {
  if (!action.meta) throw new Error('No ID provided')
  const response = yield call(api.updateNote, action.meta, action.payload.form)
  action.payload.resolve(response)
}

export function* createNoteSaga(api: ApiDefinition, action: ExactAction<NoteAction>): Saga {
  yield call(errorWrapper, createNote, api, action)
}
export function* fetchNoteSaga(api: ApiDefinition, action: ExactAction<number>): Saga {
  yield call(errorWrapper, fetchNote, api, action)
}
export function* fetchNotesSaga(api: ApiDefinition, action: Action): Saga {
  yield call(errorWrapper, fetchNotes, api, action)
}
export function* removeNoteSaga(api: ApiDefinition, action: ExactAction<NoteDeleteAction>): Saga {
  yield call(errorWrapper, removeNote, api, action)
}
export function* updateNoteSaga(api: ApiDefinition, action: ExactAction<NoteAction, number>): Saga {
  yield call(errorWrapper, updateNote, api, action)
}
