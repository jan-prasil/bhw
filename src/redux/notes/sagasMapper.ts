import { Effect, takeEvery } from 'redux-saga/effects'

import { createNote, fetchNote, fetchNotes, removeNote, updateNote } from './reducer'
import {
  createNoteSaga,
  fetchNoteSaga,
  fetchNotesSaga,
  removeNoteSaga,
  updateNoteSaga,
} from './sagas'
import { ApiDefinition } from '../../types'

export default (api: ApiDefinition): Effect[] => [
  takeEvery(createNote, createNoteSaga, api),
  takeEvery(fetchNote, fetchNoteSaga, api),
  takeEvery(fetchNotes, fetchNotesSaga, api),
  takeEvery(removeNote, removeNoteSaga, api),
  takeEvery(updateNote, updateNoteSaga, api),
]
