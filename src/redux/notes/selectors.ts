import NoteList from '../../models/NoteList'
import { NoteDetail, State } from '../../types'

export const getNote = (id: number) => (state: State): NoteDetail =>
  state.notes.getIn(['items', id.toString()])

export const getNoteList = (state: State): NoteList =>
  new NoteList(state.notes.getIn(['noteList']).asMutable())
