import Immutable from 'seamless-immutable'

import { ImmutableNoteState, NoteState } from '../../types'

export const defaultState: NoteState = {
  noteList: [],
  items: {},
}

const initialState: ImmutableNoteState = Immutable.from(defaultState)
export default initialState
