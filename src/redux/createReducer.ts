import { connectRouter } from 'connected-react-router/seamless-immutable'
import { History } from 'history'
import { AnyAction, combineReducers, Reducer } from 'redux'

import configReducer from './config'
import notesReducer from './notes'
import pendingReducer from './pending'
import { MutableState } from '../types'

interface CreateReducerOptions {
  history: History<unknown>
}

const createReducer = ({ history }: CreateReducerOptions): Reducer<MutableState, AnyAction> =>
  combineReducers<MutableState>({
    config: configReducer,
    pending: pendingReducer,
    router: connectRouter(history),
    notes: notesReducer,
  })

export default createReducer
