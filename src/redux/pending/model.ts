import Immutable from 'seamless-immutable'

import { ImmutablePendingState, PendingState } from '../../types/redux/pending'

const defaultState: PendingState = {
  sagas: {},
}

const initialState: ImmutablePendingState = Immutable.from(defaultState)
export default initialState
