import { createActions, handleActions } from 'redux-actions'

import initialState from './model'
import { Action, ActionCreator } from '../../types'
import { ImmutablePendingState } from '../../types/redux/pending'

export const actions = createActions('ADD_PENDING_SAGA', 'REMOVE_PENDING_SAGA')

export const { addPendingSaga, removePendingSaga }: ActionCreator = actions

export default handleActions<ImmutablePendingState>(
  {
    [addPendingSaga]: (state, action: Action<any>) =>
      state.updateIn(['sagas', action.payload], (data: number) => (data ? data + 1 : 1)),
    [removePendingSaga]: (state, action: Action<any>) =>
      state.updateIn(['sagas', action.payload], (data: number) =>
        data > 1 ? data - 1 : undefined
      ),
  },
  initialState
)
