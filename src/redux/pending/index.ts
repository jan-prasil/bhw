import * as pendingActions from './reducer'
import * as pendingSelectors from './selectors'

export { pendingActions, pendingSelectors }

export default pendingActions.default
