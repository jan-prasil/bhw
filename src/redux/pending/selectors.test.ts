import Immutable from 'seamless-immutable'

import { isSagaPending } from './selectors'

const action = () => ({
  type: 'test',
})

describe('pending selectors', () => {
  it('isSagaPending true', () => {
    const state = Immutable<any>({
      pending: {
        sagas: {
          test: 2,
        },
      },
    })
    expect(isSagaPending(state, action)).toBeTruthy()
  })

  it('isSagaPending false', () => {
    const state = Immutable<any>({
      pending: {
        sagas: {},
      },
    })
    expect(isSagaPending(state, action)).toBeFalsy()
  })
})
