import get from 'lodash/get'

import { Action, State } from '../../types'

export const isSagaPending = (state: State, action: (arg?: any) => Action): boolean =>
  get(state, `pending.sagas.${action().type}`, 0) > 0
