import Immutable from 'seamless-immutable'

import initialState from './model'
import handleActions, { addPendingSaga, removePendingSaga } from './reducer'

describe('pending reducer', () => {
  it('returns the initial state', () => {
    const state = handleActions(initialState, { payload: {}, type: 'TEST' })
    expect(state).toEqual(initialState)
  })

  describe('actions', () => {
    it('addPendingSaga', () => {
      const state = Immutable<any>({
        sagas: {
          test: 1,
        },
      })

      expect(handleActions(initialState, addPendingSaga('test'))).toEqual(state)
    })

    it('removePendingSaga', () => {
      const state = Immutable<any>({
        sagas: {
          test: 1,
        },
      })

      const stateWithPendingSaga = initialState.setIn(['sagas', 'test'], 2)

      expect(handleActions(stateWithPendingSaga, removePendingSaga('test'))).toEqual(state)
    })
  })
})
