import { Endpoints } from '../types'

const endpoints: Endpoints = {
  fetchConfig: () => '/config.json',
  fetchNotes: () => '/notes',
  deleteNote: ({ id } = {}) => `/notes/${id}`,
  fetchNoteById: ({ id } = {}) => `/notes/${id}`,
  postNote: () => '/notes',
  updateNote: ({ id } = {}) => `/notes/${id}`,
}

export default endpoints
