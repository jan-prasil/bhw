import React from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { shallowEqual } from 'recompose'
import { Subtract } from 'utility-types'

import { pendingSelectors } from '../../redux/pending'
import { Action, State } from '../../types'

export type PrefetchProps = {
  pending: boolean
}

type PrefetchFunctionAction<P> = {
  prefetch: (arg: any) => Action
  getPayload: (props: P) => any
}
type PrefetchAction<P> = (() => Action) | PrefetchFunctionAction<P>

export const prefetch = <P extends PrefetchProps>(
  actions: Array<PrefetchAction<Subtract<P, PrefetchProps>>>
) => (Component: React.ComponentType<P>): React.FC<Subtract<P, PrefetchProps>> => {
  const pendingSelector = (state: State) =>
    !!actions
      .map((action: PrefetchAction<P>) =>
        pendingSelectors.isSagaPending(
          state,
          typeof action === 'function' ? action : action.prefetch
        )
      )
      .filter((value) => value).length

  return (props: Subtract<P, PrefetchProps>) => {
    const [initialPending, setInitialPending] = React.useState(true)
    const pending = useSelector(pendingSelector, shallowEqual)
    const dispatch = useDispatch()
    const preloadData = React.useCallback(() => {
      actions.map((action) =>
        dispatch(
          typeof action === 'function' ? action() : action.prefetch(action.getPayload(props))
        )
      )
      setInitialPending(false)
    }, [])

    React.useEffect(() => {
      preloadData()
    }, [])

    return <Component {...(props as P)} pending={initialPending || pending} />
  }
}

export default prefetch
