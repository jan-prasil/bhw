import React from 'react'
import { FormProvider, FormState, useForm } from 'react-hook-form'
import { Subtract } from 'utility-types'

type Props = unknown

export type OnSubmitFunction = (props: {
  form: Record<string, unknown>
  resolve: (data: Record<string, unknown>) => void
}) => void

export type FormProps = {
  isSubmitting: boolean
  formState: FormState<Record<string, unknown>>
  formErrors: Array<Record<string, unknown>>
  handleSubmit: (
    onSubmit: OnSubmitFunction,
    onSuccess: ((formField: Record<string, unknown>) => void) | undefined
  ) => void
  isValid: boolean
}

export const withForm = <P extends FormProps, F>(
  Component: React.ComponentType<P>
): React.FC<Subtract<P, FormProps> & Props> => (props: Props) => {
  const methods = useForm<F>({ mode: 'onChange' })
  const [formErrors, setFormErrors] = React.useState([])
  const [isSubmitting, setSubmitting] = React.useState(false)

  const handleSubmit = React.useCallback(
    (onSubmit, onSuccess) => {
      setSubmitting(true)
      methods.handleSubmit((data: Record<string, unknown>) =>
        new Promise((resolve) => {
          onSubmit({
            form: data,
            resolve,
          })
        }).then((formFields: unknown) => {
          try {
            // here we can make async validation and throw an exception in case of a validation error
            if (onSuccess) onSuccess(formFields)
          } catch (error) {
            // here we can handle errors
            setFormErrors([])
          }
          setSubmitting(false)
        })
      )()
    },
    [methods]
  )

  return (
    <FormProvider {...methods}>
      <Component
        {...(props as P)}
        isSubmitting={isSubmitting}
        formState={methods.formState}
        formErrors={formErrors}
        handleSubmit={handleSubmit}
        isValid={methods.formState.isValid}
      />
    </FormProvider>
  )
}

export default withForm
