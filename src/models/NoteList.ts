import { NoteListItem } from '../types'

class NoteList {
  constructor(data: Array<NoteListItem>) {
    this.data = [...data]
  }

  data: Array<NoteListItem>

  getCropped(count: number): Array<NoteListItem> {
    return (this.data || []).slice(0, count)
  }
}

export default NoteList
