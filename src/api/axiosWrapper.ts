import axios, { AxiosInstance, AxiosResponse } from 'axios'

import { ApiConfig, ApiRequests } from '../types'

const createAxiosWrapper = (config: ApiConfig): AxiosInstance => {
  const api: AxiosInstance = axios.create({
    baseURL: config.apiUrl,
  })

  api.interceptors.response.use((response: AxiosResponse) => {
    const status = response.status.toString()
    if (status.startsWith('2') || status.startsWith('3')) return response.data
    throw new Error(`Returned error code: ${status}`)
  })

  return api
}

const axiosWrapper = (config: ApiConfig): ApiRequests => {
  const api = createAxiosWrapper(config)
  return {
    remove: (url, params) => api.delete(url, { params }),
    get: (url, params) => api.get(url, { params }),
    post: (url, data) => api.post(url, data),
    put: (url, data) => api.put(url, data),
  }
}

export default axiosWrapper
