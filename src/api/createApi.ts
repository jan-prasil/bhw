import { endpoints } from '../constants'
import { ApiDefinition, ApiRequests, NoteFormModel } from '../types'

const createApi = ({ get, post, put, remove }: ApiRequests): ApiDefinition => ({
  fetchConfig: () => get(endpoints.fetchConfig()),
  fetchNotes: () => get(endpoints.fetchNotes()),
  deleteNote: (id: number) => remove(endpoints.deleteNote({ id })),
  fetchNoteById: (id: number) => get(endpoints.deleteNote({ id })),
  postNote: (data: NoteFormModel) => post<NoteFormModel>(endpoints.postNote(), data),
  updateNote: (id: number, data: NoteFormModel) =>
    put<NoteFormModel>(endpoints.updateNote({ id }), data),
})

export default createApi
