import './messages/i18n'

import React from 'react'
import { createBrowserHistory } from 'history'
import ReactDOM from 'react-dom'

import configureStore from './redux/configureStore'
import createSagaManager from './redux/createSagaManager'
import RootApp from './RootApp'
import * as serviceWorker from './serviceWorker'
import 'antd/dist/antd.css'
import './containers/App/index.css'

const prepareApp = () => {
  const history = createBrowserHistory()
  const { sagaMiddleware, store } = configureStore({ history })
  const sagaManager = createSagaManager({ sagaMiddleware, store })
  sagaManager.start()

  return { history, sagaManager, store }
}

const renderApp = () => {
  const { history, sagaManager, store } = prepareApp()
  ReactDOM.render(
    <React.StrictMode>
      <RootApp history={history} sagaManager={sagaManager} store={store} />
    </React.StrictMode>,
    document.getElementById('root')
  )
}

renderApp()

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister()
