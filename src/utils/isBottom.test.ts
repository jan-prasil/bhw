import isBottom from './isBottom'

describe('isBottom util function', () => {
  it('returns false when the anchor element is not defined', () => {
    expect(isBottom(null)).toEqual(false)
  })

  it('returns false when element bottom is not on the window bottom', () => {
    const window = { innerHeight: 768 }
    const bottom = 500
    const getBoundingClientRect = jest.fn().mockReturnValue({ bottom })
    const element = {
      getBoundingClientRect,
    }

    expect(isBottom(element, window)).toEqual(false)
  })

  it('returns true when element bottom is on the window bottom', () => {
    const window = { innerHeight: 768 }
    const bottom = 768
    const getBoundingClientRect = jest.fn().mockReturnValue({ bottom })
    const element = {
      getBoundingClientRect,
    }

    expect(isBottom(element, window)).toEqual(true)
  })
})
