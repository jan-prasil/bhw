const isBottom = (element: HTMLDivElement, window: Window): boolean =>
  element ? element.getBoundingClientRect().bottom > window.innerHeight - 20 : false
export default isBottom
