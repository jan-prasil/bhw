import React, { Suspense } from 'react'
import { useSelector } from 'react-redux'
import { Route } from 'react-router-dom'

import LocalizedApp from './LocalizedApp'
import { Layout, Spinner } from '../../components'
import { configSelectors } from '../../redux/config'

const App: React.FC = () => {
  const isRootSagaStarted: boolean = useSelector(configSelectors.getRootSagaStarted)
  return (
    <Suspense fallback={Spinner}>
      {isRootSagaStarted ? (
        <Layout>
          <Route path="/:lang(cs)?" component={LocalizedApp} />
        </Layout>
      ) : null}
    </Suspense>
  )
}

export default App
