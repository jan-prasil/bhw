import React from 'react'
import { useTranslation } from 'react-i18next'
import { Route, Switch, useRouteMatch } from 'react-router-dom'

import { CreateNote, EditNote, ErrorPage, NoteDetail, NotesList } from '..'

const LocalizedApp: React.FC = () => {
  const { i18n, t } = useTranslation()
  const match = useRouteMatch()
  React.useEffect(() => {
    const params = match.params as { lang: string }
    if (params.lang) i18n.changeLanguage(params.lang)
  }, [match.params, i18n])

  return (
    <Switch>
      <Route path={t('routes.base')} exact component={NotesList} />
      <Route path={t('routes.createNote')} exact component={CreateNote} />
      <Route path={t('routes.editNote')} exact component={EditNote} />
      <Route path={t('routes.showDetail')} component={NoteDetail} key="detail" />
      <Route path={t('routes.error')} component={ErrorPage} />
    </Switch>
  )
}

export default LocalizedApp
