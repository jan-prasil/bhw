import React from 'react'
import { Result } from 'antd'
import { useTranslation } from 'react-i18next'

const Error: React.FC = () => {
  const { t } = useTranslation()
  return <Result status="error" title={t('error.oooops')} />
}

export default Error
