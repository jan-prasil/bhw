import React from 'react'
import { push } from 'connected-react-router'
import { useTranslation } from 'react-i18next'
import { useDispatch } from 'react-redux'
import { RouteComponentProps, withRouter } from 'react-router'
import { compose } from 'recompose'

import EditComponent from './Edit.comp'
import { withForm } from '../../../decorators'
import { FormProps } from '../../../decorators/form'
import { PrefetchProps } from '../../../decorators/prefetch'
import { NoteForm } from '../../../forms'
import { noteActions } from '../../../redux/notes'
import useNoteFetchComponent from '../hooks/useNoteFetchComponent'

type Props = FormProps & RouteComponentProps & PrefetchProps

const EditNote: React.FC<Props> = ({
  handleSubmit,
  isSubmitting,
  isValid,
  match,
}: // pending,
Props) => {
  const { t } = useTranslation()
  const id = React.useMemo(() => (match.params as { id: number }).id, [match])
  const FetchedNoteComponent = useNoteFetchComponent(id)

  const dispatch = useDispatch()
  const onSubmit = () =>
    handleSubmit(
      (form) => dispatch(noteActions.updateNote(form, id)),
      () =>
        dispatch(
          push({
            pathname: t('routes.showDetail').replace(':id', id.toString()),
            state: 'success-update',
          })
        )
    )

  return (
    <FetchedNoteComponent>
      {(data: any, pending: boolean) => (
        <EditComponent
          isSubmitting={isSubmitting}
          isValid={isValid && !pending}
          onSubmit={onSubmit}>
          <NoteForm pending={pending} initialData={data} />
        </EditComponent>
      )}
    </FetchedNoteComponent>
  )
}

const enhance = compose<Props, unknown>(withRouter, withForm)

export default enhance(EditNote)
