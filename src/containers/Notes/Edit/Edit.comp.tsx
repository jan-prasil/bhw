import React from 'react'
import { Button, Col, Row } from 'antd'
import Title from 'antd/lib/typography/Title'
import { useTranslation } from 'react-i18next'

import styles from './Edit.module.css'

interface Props {
  children: React.ReactChild
  isValid: boolean
  isSubmitting: boolean
  onSubmit: () => void
}

const EditComponent: React.FC<Props> = ({ children, isValid, isSubmitting, onSubmit }: Props) => {
  const { t } = useTranslation()
  return (
    <Row>
      <Col xs={24}>
        <Title level={4}>{t('edit.Edit existing note')}</Title>
      </Col>
      <Col xs={24} className={styles.space}>
        {children}
      </Col>
      <Col xs={24}>
        <Button disabled={!isValid || isSubmitting} loading={isSubmitting} onClick={onSubmit}>
          {t('edit.Update note')}
        </Button>
      </Col>
    </Row>
  )
}

export default EditComponent
