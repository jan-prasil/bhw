import React from 'react'
import { Button, Col, Row, Skeleton, Space } from 'antd'
import Title from 'antd/lib/typography/Title'
import { useTranslation } from 'react-i18next'
import { Link } from 'react-router-dom'

import { NoteDetail } from '../../../types'

interface Props {
  data: NoteDetail
  id: string
  pending: boolean
}

const DetailComponent: React.FC<Props> = ({ data, id, pending }: Props) => {
  const { t } = useTranslation()

  return (
    <Row>
      <Col xs={24}>
        {pending && <Skeleton title paragraph={false} />}
        {!pending && <Title level={4}>{data.title}</Title>}
      </Col>
      <Col xs={24}>
        <Space>
          <Link to={t('routes.removeNote').replace(':id', id)}>
            <Button disabled={pending}>{t('detail.Remove')}</Button>
          </Link>
          <Link to={t('routes.editNote').replace(':id', id)}>
            <Button disabled={pending}>{t('detail.Edit')}</Button>
          </Link>
        </Space>
      </Col>
    </Row>
  )
}

export default DetailComponent
