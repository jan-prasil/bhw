import React from 'react'
import { useTranslation } from 'react-i18next'
import { Route, RouteComponentProps, withRouter } from 'react-router'
import { compose } from 'recompose'

import NoteDetailComponent from './Detail.comp'
import { PrefetchProps } from '../../../decorators/prefetch'
import useNoteFetchComponent from '../hooks/useNoteFetchComponent'
import useRemoveModal from '../hooks/useRemoveModal'

type Props = RouteComponentProps & PrefetchProps

const NoteDetail: React.FC<Props> = ({ match }: Props) => {
  const { t } = useTranslation()
  const id = React.useMemo(() => (match.params as { id: number }).id, [match])
  const FetchedNoteComponent = useNoteFetchComponent(id)
  const Modal = useRemoveModal(id)

  return (
    <FetchedNoteComponent>
      {(data: any, pending: boolean) => [
        <Route path={t('routes.removeNote')} component={Modal} key="detail" />,
        <NoteDetailComponent id={id.toString()} data={data} pending={pending} />,
      ]}
    </FetchedNoteComponent>
  )
}

const enhance = compose<Props, unknown>(withRouter)

export default enhance(NoteDetail)
