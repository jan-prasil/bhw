import React from 'react'
import { Button, Col, Row, Space } from 'antd'
import Title from 'antd/lib/typography/Title'
import { useTranslation } from 'react-i18next'

interface Props {
  children: React.ReactChild
  isValid: boolean
  isSubmitting: boolean
  onSubmit: () => void
}

const CreateComponent: React.FC<Props> = ({ children, isValid, isSubmitting, onSubmit }: Props) => {
  const { t } = useTranslation()
  return (
    <Row>
      <Space direction="vertical" style={{ width: '100%' }}>
        <Col xs={24}>
          <Title level={4}>{t('create.Create new note')}</Title>
        </Col>
        <Col xs={24}>{children}</Col>
        <Col xs={24}>
          <Button disabled={!isValid || isSubmitting} loading={isSubmitting} onClick={onSubmit}>
            {t('create.Create note')}
          </Button>
        </Col>
      </Space>
    </Row>
  )
}

export default CreateComponent
