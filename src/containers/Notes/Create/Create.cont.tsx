import React from 'react'
import { push } from 'connected-react-router'
import { useTranslation } from 'react-i18next'
import { useDispatch } from 'react-redux'

import CreateComponent from './Create.comp'
import { withForm } from '../../../decorators'
import { FormProps } from '../../../decorators/form'
import { NoteForm } from '../../../forms'
import { noteActions } from '../../../redux/notes'

type Props = FormProps

const CreateNote: React.FC<Props> = ({ handleSubmit, isSubmitting, isValid }: Props) => {
  const { t } = useTranslation()
  const dispatch = useDispatch()
  const onSubmit = () =>
    handleSubmit(
      (form) => dispatch(noteActions.createNote(form)),
      () => dispatch(push({ pathname: t('routes.base'), state: 'success-create' }))
    )
  return (
    <CreateComponent isSubmitting={isSubmitting} isValid={isValid} onSubmit={onSubmit}>
      <NoteForm />
    </CreateComponent>
  )
}

export default withForm(CreateNote)
