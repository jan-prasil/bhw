import React from 'react'
import { EditOutlined, FileTextOutlined } from '@ant-design/icons'
import { Button, Col, Row, Space } from 'antd'
import Text from 'antd/lib/typography/Text'
import { useTranslation } from 'react-i18next'
import { Link } from 'react-router-dom'

import { Spinner } from '../../../components'
import { NoteListItem } from '../../../types'
import styles from './List.module.css'

interface Props {
  notes: Array<NoteListItem>
  showLoader: boolean
}

const ListComponent: React.FC<Props> = ({ notes, showLoader }: Props) => {
  const { t } = useTranslation()
  return (
    <Row>
      {notes.map((note) => (
        <Col xs={24} className={styles.listItem}>
          <Text className={styles.text}>{note.title}</Text>
          <Space>
            <Link to={t('routes.showDetail').replace(':id', note.id.toString())}>
              <Button type="link" icon={<FileTextOutlined />}>
                {t('list.Show detail')}
              </Button>
            </Link>
            <Link to={t('routes.editNote').replace(':id', note.id.toString())}>
              <Button type="link" icon={<EditOutlined />}>
                {t('list.Edit note')}
              </Button>
            </Link>
          </Space>
        </Col>
      ))}
      {showLoader && <Spinner />}
    </Row>
  )
}
export default ListComponent
