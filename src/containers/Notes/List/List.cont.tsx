import React from 'react'
import { useSelector } from 'react-redux'

import ListComponent from './List.comp'
import { Spinner } from '../../../components'
import { prefetch } from '../../../decorators'
import { noteActions, noteSelectors } from '../../../redux/notes'
import useScrollItems from '../hooks/useScrollItems'

interface Props {
  pending: boolean
}

const NotesList: React.FC<Props> = ({ pending }: Props) => {
  const rowRef = React.useRef() as React.MutableRefObject<HTMLDivElement>
  const { showCount } = useScrollItems(10, rowRef.current)
  const notes = useSelector(noteSelectors.getNoteList)
  const croppedNotes = React.useMemo(() => notes.getCropped(showCount), [notes, showCount])

  return (
    <div ref={rowRef}>
      {pending && <Spinner />}
      {!pending && (
        <ListComponent notes={croppedNotes} showLoader={showCount < notes.data.length} />
      )}
    </div>
  )
}

export default prefetch([noteActions.fetchNotes])(NotesList)
