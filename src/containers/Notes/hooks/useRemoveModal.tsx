import React from 'react'
import { Modal } from 'antd'
import { push } from 'connected-react-router'
import { useTranslation } from 'react-i18next'
import { useDispatch } from 'react-redux'

import { noteActions } from '../../../redux/notes'

const useRemoveModal = (id: number): React.FC => {
  const { t } = useTranslation()
  const dispatch = useDispatch()
  const close = () => dispatch(push(t('routes.showDetail').replace(':id', id.toString())))
  const remove = () => {
    dispatch(
      noteActions.removeNote({
        id,
        resolve: () => dispatch(push({ pathname: t('routes.base'), state: 'success-remove' })),
      })
    )
  }

  return () => (
    <Modal
      title={t('remove.Confirmation')}
      visible
      onOk={remove}
      confirmLoading={false}
      onCancel={close}>
      {t('remove.Really want to remove this note?')}
    </Modal>
  )
}

export default useRemoveModal
