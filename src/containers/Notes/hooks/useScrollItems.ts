import React from 'react'

import isBottom from '../../../utils/isBottom'

interface ScrollItems {
  showCount: number
}

const useScrollItems = (base: number, element: HTMLDivElement): ScrollItems => {
  const [showCount, setShowCount] = React.useState(base)

  const handleScroll = () => {
    if (isBottom(element, window)) {
      setShowCount((currentShowCount) => currentShowCount + base)
    }
  }

  React.useEffect(() => {
    document.addEventListener('scroll', handleScroll)
    return () => document.removeEventListener('scroll', handleScroll)
  })

  return { showCount }
}

export default useScrollItems
