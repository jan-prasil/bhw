import React from 'react'
import { useSelector } from 'react-redux'

import { prefetch } from '../../../decorators'
import { PrefetchProps } from '../../../decorators/prefetch'
import { noteActions, noteSelectors } from '../../../redux/notes'

interface ChildrenType {
  children: (data: any, pending: boolean) => React.ReactNode
}

type Props = PrefetchProps & ChildrenType

const Note = (id: number): React.FC<Props> => ({ children, pending }: Props) => {
  const data = useSelector(noteSelectors.getNote(id)) // eslint-disable-line
  return <div>{children(data, pending)}</div>
}

const useNoteFetchComponent = (id: number): React.FC<ChildrenType> =>
  React.useMemo(
    () =>
      prefetch<Props>([
        {
          prefetch: noteActions.fetchNote,
          getPayload: () => id,
        },
      ])(Note(id)),
    [id]
  )

export default useNoteFetchComponent
