import React from 'react'
import { EditOutlined } from '@ant-design/icons'
import { Col, Form, Input, Skeleton } from 'antd'
import { Controller, useFormContext } from 'react-hook-form'
import { useTranslation } from 'react-i18next'

import { NoteDetail } from '../../types'

interface Props {
  initialData?: NoteDetail
  pending?: boolean
}

const NoteForm: React.FC<Props> = ({ initialData, pending }: Props) => {
  const { t } = useTranslation()
  const { control, errors } = useFormContext()

  return (
    <Col xs={24}>
      <Form.Item
        label={t('noteForm.Text of the note')}
        validateStatus={errors?.title ? 'error' : undefined}
        help={errors?.title?.message}>
        {pending && <Skeleton title paragraph={false} />}
        {!pending && (
          <Controller
            as={Input}
            control={control}
            rules={{
              required: t('common.required').toString(),
            }}
            name="title"
            defaultValue={initialData?.title}
            prefix={<EditOutlined />}
          />
        )}
      </Form.Item>
    </Col>
  )
}

NoteForm.defaultProps = {
  initialData: {
    id: 0,
    title: '',
  },
  pending: false,
}

export default NoteForm
