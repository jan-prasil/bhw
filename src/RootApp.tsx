import React from 'react'
import { ConnectedRouter } from 'connected-react-router/seamless-immutable'
import { History } from 'history'
import { Provider } from 'react-redux'
import { Store } from 'redux'

import App from './containers/App/App'
import { MutableState, SagaManager } from './types'

interface Props {
  store: Store<MutableState>
  history: History<unknown>
  sagaManager: SagaManager
}

const RootApp: React.FC<Props> = ({ sagaManager, store, history }: Props) => {
  React.useEffect(() => () => sagaManager.cancel(), [sagaManager])

  return (
    <Provider store={store}>
      <ConnectedRouter history={history}>
        <App />
      </ConnectedRouter>
    </Provider>
  )
}

export default RootApp
