export interface NodeModule {
  hot: unknown
}

export interface Payload {
  [x: string]: unknown
}

declare global {
  interface Window {
    __REDUX_DEVTOOLS_EXTENSION_COMPOSE__: unknown
  }

  interface Store {
    sagaMiddleware: unknown
  }
}
