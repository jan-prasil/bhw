export type FormErrorMessages = {
  /** error messages for the entire form */
  messages: Array<FormError>
}

export type FormError = {
  /** text message */
  text: string
  /** error type */
  type: string
}

export type FormField = {
  errorMessages?: Array<{ errorText: string }>
  name: string
  state?: string
  value: any
}

export type FormFieldsInnerData = {
  [formInputName: string]: FormField
}

export type FormFieldsInputData = {
  [formInputName: string]: FormField
} & FormFieldStatus

export type FormFieldsInputDataWithStatus = {
  [formInputName: string]: FormField
} & FormFieldStatus

export type FormFieldStatusInner = {
  messages: Array<{ code: string; text: string; type: string }>
}

export type FormFieldStatus = {
  formStatus: ?FormFieldStatusInner
}

export type FormFields = {
  [formName: string]: FormFieldsInnerData
}

export type FormConfig = {
  name: string
}
