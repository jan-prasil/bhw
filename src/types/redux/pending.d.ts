import { Immutable } from 'seamless-immutable'

interface PendingState {
  sagas: Record<string, number>
}

export type ImmutablePendingState = Immutable<PendingState>
