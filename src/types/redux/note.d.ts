import { Immutable } from 'seamless-immutable'

export interface NoteState {
  noteList: Array<NoteListItem>
  items: Record<string, NoteDetail>
}

export type ImmutableNoteState = Immutable<NoteState>

export interface NoteListItem {
  id: number
  title: string
}

export interface NoteDetail {
  id: number
  title: string
}

export interface NoteFormModel {
  id: number
  title: string
}

export interface NoteDeleteAction {
  resolve: () => void
  id: number
}

export interface NoteAction {
  resolve: (response: unknown) => void
  form: NoteFormModel
}
