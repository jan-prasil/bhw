import { Immutable } from 'seamless-immutable'

export interface ConfigState {
  apiUrl: string
  rootSagaStarted: boolean
}

export type ImmutableConfigState = Immutable<ConfigState>

export interface Config {
  apiUrl: string
}
