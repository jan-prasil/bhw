import { RouterState } from 'connected-react-router'
import { AnyAction, Store } from 'redux'
import { SagaMiddleware, Task } from 'redux-saga'
import { Immutable } from 'seamless-immutable'

import { ImmutableConfigState } from './config'
import { ImmutableNoteState } from './note'
import { ImmutablePendingState } from './pending'

interface Payload {
  [x: string]: unknown
}

type Meta = Record<string, unknown>

export interface FormPayload {
  form: Record<string, unknown>
  resolve: (data: Record<string, unknown>) => void
  reject: () => void
}

export interface MutableState {
  config: ImmutableConfigState
  notes: ImmutableNoteState
  pending: ImmutablePendingState
  router: RouterState
}

export interface ActionCreator {
  [actionName: string]: any
}

export interface Action<T = Payload, M = Meta> {
  type: string
  payload: T | string | number
  meta?: M
}

export interface ExactAction<T = Payload, M = Meta> {
  type: string
  payload: T
  meta?: M
}

export type Dispatch = (action: Action) => void

export type State = Immutable<MutableState>

export interface StoreConfiguration {
  sagaMiddleware: SagaMiddleware
  store: Store<MutableState, AnyAction>
}

export interface SagaManager {
  cancel: () => void
  start: () => Task
}

export type Saga = Generator<*, void, *>

export type SagaWithReturn = Generator<*, any, *>
