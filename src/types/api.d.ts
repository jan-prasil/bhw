import { NoteFormModel } from './redux/note'

export type ApiConfig = {
  apiUrl: string
}

export interface ApiDefinition {
  fetchConfig: () => Promise<any>
  fetchNotes: () => Promise<any>
  fetchNoteById: (id: number) => Promise<any>
  postNote: (data: NoteFormModel) => Promise<any>
  updateNote: (id: number, data: NoteFormModel) => Promise<any>
  deleteNote: (id: number) => Promise<any>
}

export interface ApiRequests {
  get: (url: string, params?: Record<string, unknown>) => Promise<unknown>
  post: <T = Record<string, unknown>>(url: string, data: T) => Promise<unknown>
  put: <T = Record<string, unknown>>(url: string, data: T) => Promise<unknown>
  remove: (url: string, params?: Record<string, unknown>) => Promise<unknown>
}

export interface Status {
  error: boolean
  done: boolean
  pending: boolean
}

export interface ApiRequest<V> {
  data: V
  status: Status
}
