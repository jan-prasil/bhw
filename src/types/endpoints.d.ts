import { $Keys } from 'utility-types'

import { ApiDefinition } from './api'

export type Endpoints = {
  [x in $Keys<ApiDefinition>]: (params?: Record<string, unknown>) => string
}
