import React from 'react'
import { PlusCircleOutlined } from '@ant-design/icons'
import { Button, Space } from 'antd'
import { useTranslation } from 'react-i18next'
import { Link } from 'react-router-dom'

import ChangeLanguage from '../ChangeLanguage/ChangeLanguage'

const Menu: React.FC = () => {
  const { t } = useTranslation()
  return (
    <Space>
      <Link to={t('routes.base').toString()}>
        <Button type="primary" shape="round" size="middle">
          {t('layout.List of notes')}
        </Button>
      </Link>
      <Link to={t('routes.createNote').toString()}>
        <Button type="primary" shape="round" icon={<PlusCircleOutlined />} size="middle">
          {t('layout.Add a note')}
        </Button>
      </Link>
      <ChangeLanguage />
    </Space>
  )
}

export default Menu
