import React from 'react'
import { Button } from 'antd'
import { push } from 'connected-react-router'
import { useTranslation } from 'react-i18next'
import { useDispatch } from 'react-redux'

const ChangeLanguage: React.FC = () => {
  const { i18n, t } = useTranslation()
  const dispatch = useDispatch()

  const changeLanguage = (lang: 'en' | 'cs') => () => {
    i18n.changeLanguage(lang).then(() => {
      dispatch(push(t('routes.base')))
    })
  }

  return (
    <div>
      {i18n.language === 'cs' && (
        <Button type="link" onClick={changeLanguage('en')}>
          {t('common.switch to')}
        </Button>
      )}
      {i18n.language !== 'cs' && (
        <Button type="link" onClick={changeLanguage('cs')}>
          {t('common.switch to')}
        </Button>
      )}
    </div>
  )
}

export default ChangeLanguage
