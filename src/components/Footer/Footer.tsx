import React from 'react'
import { Footer } from 'antd/lib/layout/layout'
import { useTranslation } from 'react-i18next'

import styles from './Footer.module.css'

const FooterComponent: React.FC = () => {
  const { t } = useTranslation()

  return (
    <Footer className={styles.footer}>
      {t('layout.Created with')}{' '}
      <span aria-label="love" role="img">
        ❤️
      </span>
    </Footer>
  )
}

export default FooterComponent
