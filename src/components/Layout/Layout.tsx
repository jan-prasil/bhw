import React from 'react'
import { Layout, message } from 'antd'
import { Content, Header } from 'antd/lib/layout/layout'
import { useTranslation } from 'react-i18next'
import { useLocation } from 'react-router-dom'

import Footer from '../Footer/Footer'
import Logo from '../Logo/Logo'
import Menu from '../Menu/Menu'
import styles from './Layout.module.css'

interface Props {
  children: React.ReactNode
}

const AppLayout: React.FC<Props> = ({ children }: Props) => {
  const location = useLocation()
  const { t } = useTranslation()

  React.useEffect(() => {
    if (location.state) message.success(t(`notifications.${location.state as string}`))
  }, [location.state])

  return (
    <Layout className={styles.app}>
      <Header className={styles.header}>
        <Logo />
        <Menu />
      </Header>
      <Content className={styles.contentBackground}>{children}</Content>
      <Footer />
    </Layout>
  )
}

export default AppLayout
