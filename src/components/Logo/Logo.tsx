import React from 'react'
import Title from 'antd/lib/typography/Title'

import styles from './Logo.module.css'

const Logo: React.FC = () => (
  <Title className={styles.logo} level={4}>
    NotesApp{' '}
    <span role="img" aria-label="pen">
      📝
    </span>
  </Title>
)

export default Logo
